#! /usr/bin/env python3
import argparse

from tornado.ioloop import IOLoop

from irookc.ircserver import Server

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--conf", default="irookc.conf")
args = parser.parse_args()

s = Server(args.conf)
s.listen()
IOLoop.instance().start()
