from tornado.ioloop import IOLoop

def errorcheck(f):
    # execute a future, doing something useful with any exception raised
    IOLoop.current().add_future(f, lambda x: x.result())

class User(str):
    def __init__(self, *args, **kwargs):
        self.lowered = self.lower()

    def __eq__(self, other):
        # case insensitive compare
        return self.lowered == other.lower()

    def __id__(self):
        return id(self.lowered)

    def __hash__(self):
        return hash(self.lowered)
