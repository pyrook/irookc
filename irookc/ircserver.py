import configparser
import functools
import logging
import re
import ssl
import textwrap
import time

from tornado.gen import coroutine
from tornado.ioloop import IOLoop
from tornado.iostream import StreamClosedError
from tornado.tcpserver import TCPServer

from .dns import DNSException, Resolver
from .helpers import errorcheck, User
from .rooklib import Rook
from . import __VERSION__

class Server(TCPServer):
    def __init__(self, config_path):
        conf_file = configparser.SafeConfigParser()
        conf_file.read(config_path)
        conf = conf_file["irookc"]

        if "ssl_cert" in conf and "ssl_key" in conf:
            ssl_options = ssl.create_default_context()
            ssl_options.set_ecdh_curve("prime256v1")
            ssl_options.options |= ssl.OP_SINGLE_ECDH_USE | ssl.OP_CIPHER_SERVER_PREFERENCE
            ssl_options.load_cert_chain(conf["ssl_cert"], conf["ssl_key"])
            # no, clients don't need certificates...
            ssl_options.check_hostname = False
            ssl_options.verify_mode=ssl.CERT_NONE
        else:
            ssl_options = None
        super().__init__(ssl_options=ssl_options)

        self.conf_port = conf.get("port", 6697)
        self.conf_addr = conf.get("bind_address", None)

        # TODO: logfile
        debuglevel = getattr(logging, conf.get("debug", "warning").upper(), None)
        if not isinstance(debuglevel, int):
            raise ValueError("Invalid log level")
        logging.basicConfig(format="{asctime} {levelname}:{message}", style="{", level=debuglevel)

        self.server_name = conf["name"]

        try:
            with open(conf["motd"]) as f:
                self.motd = ["- " + line for para in f.read().split("\n") for line in textwrap.wrap(para)]
        except (KeyError, OSError):
            logging.warning("No MOTD file available")
            self.motd = ["- No MOTD -"]

        self.rook = Rook(conf["url"])

        self.clients_by_addr = {}
        self.clients_by_name = {}

    def listen(self):
        return super().listen(self.conf_port, address=self.conf_addr)

    def handle_stream(self, stream, address):
        # can we ever get a new connection with the same ip,port tuple as an
        # existing one? let's assume not...
        logging.info("Client connected from %s", address)
        c = ClientHandler(stream, address[0], self)
        self.clients_by_addr[address] = c
        self.io_loop.add_future(c.run(), functools.partial(self.cleanup, address))

    def cleanup(self, address, res):
        client = self.clients_by_addr[address]
        if not client.stream.closed():
            client.stream.close()
        del self.clients_by_addr[address]
        if client.nickname and client.nickname in self.clients_by_name:
            del self.clients_by_name[client.nickname]
        errorcheck(client.cleanup())
        try:
            out = res.result()
        except StreamClosedError:
            logging.info("Client disconnected from %s", address)
        except Exception:
            logging.exception("Connection closed with unexpected error")
        else:
            logging.info("Closed connection for %s", address)


class ClientHandler:
    ignored_commands = {"cap", "ison", "pong"}
    def __init__(self, stream, address, server):
        self.stream = stream
        self.server = server

        # state
        self.nickname = None
        self.user = None
        self.password = None
        # we don't show real hosts to most users per RookChat policy
        self.real_host = None
        self.display_host = None
        self.ip = address
        self.rooms = {}
        self.last_contact = 0
        self.timeout = None
        self.rook = None

    @coroutine
    def run(self):
        try:
            self.real_host = yield Resolver().gethostbyaddr(self.ip)
        except DNSException:
            self.real_host = self.ip
        logging.info("DNS: %s: %s", self.ip, self.real_host)
        while True:
            data = yield self.stream.read_until(b"\n")
            # note that we don't wait for this Future to complete to avoid this
            # client's IRC listen loop blocking waiting for HTTP requests which
            # may be invoked on the RookChat server to complete
            errorcheck(self.handle_msg(data))

    @coroutine
    def cleanup(self):
        logging.info("Cleanup")
        for room in self.rooms.values():
            errorcheck(room.leave(confirm=False))
        if self.timeout:
            self.server.io_loop.remove_timeout(self.timeout)

    timeout_length = 60
    def set_timeout(self):
        if self.timeout:
            self.server.io_loop.remove_timeout(self.timeout)
        self.timeout = self.server.io_loop.call_later(self.timeout_length, self.handle_timeout)

    @coroutine
    def handle_timeout(self):
        now = time.time()
        if (now - self.last_contact) < self.timeout_length:
            # called by accident, nothing to do
            return
        if self.timeout_length <= (now - self.last_contact) < (self.timeout_length * 2):
            # ping the client
            logging.debug("Pinging client")
            yield self.send_msg("PING", actor="")
            self.set_timeout()
            return
        # client has not responded to ping
        logging.info("Client timed out")
        self.stream.close()

    @coroutine
    def handle_msg(self, data_bytes):
        self.set_timeout()
        self.last_contact = time.time()
        # IRC clients usually default (sensibly) to UTF8
        try:
            data = data_bytes.decode("utf8")
        except UnicodeDecodeError:
            data = data_bytes.decode("iso-8859-1", errors="replace")
        m = re.match(r"(?::([^ ]+) )?((?:[^ :][^ \r]* ?)*?)(?: :([^\r\n]+))?\r?\n", data)
        if not m:
            logging.warning("Failed to parse message: %s", data_bytes)
            return
        if not m.group(2):
            logging.warning("Failed to parse message - no contents: %s", data)
            return
        cmd, *args = m.group(2).split()
        if m.group(3):
            args.append(m.group(3))
        if self.rook is None:
            # only login commands are allowed before a user has logged in
            if cmd.lower() not in ("user", "pass", "nick"):
                logging.info("Ignoring command from non-logged-in-user: %s", cmd)
                return
        handler = getattr(self, "cmd_{}".format(cmd.lower()), None)
        if not handler:
            if cmd.lower() in self.ignored_commands:
                logging.debug("Ignored command: %s", cmd)
            else:
                yield self.handle_cmd_unknown(cmd, *args)
        else:
            yield handler(*args)

    @coroutine
    def send_msg(self, cmd, *args, actor=None, target=None, colon=True):
        """Send a message to the client.

        This is built up as follows:
        :actor cmd target args[:-1] :args[-1]
        If not specified, actor is the server's name.
        If not specified, target is the client's nickname.
        Only the last item of args may contain spaces, and will get a colon
        prepended if it does."""
        args = list(args)
        if target is None:
            target = self.nickname
        if actor is None:
            actor = self.server.server_name
        else:
            if isinstance(actor, tuple):
                actor = "{}!{}@{}".format(*actor)
            elif isinstance(actor, User):
                actor = "{0}!{0}@{0}".format(actor)
            elif isinstance(actor, ClientHandler):
                actor = "{}!{}@{}".format(actor.nickname, actor.user, actor.display_host)
        if args and not args[-1].startswith(":") and colon:
            args[-1] = ":" + args[-1]
        msg = cmd + " " + target + " " + " ".join(args)
        if actor:
            msg = ":{} {}".format(actor, msg)
        to_send = (msg.strip() + "\r\n").encode("utf8")
        #logging.debug("sending message %s", to_send)
        yield self.stream.write(to_send)

    @coroutine
    def send_error(self, error):
        yield self.send_msg("NOTICE", error)

    @coroutine
    def check_login(self):
        if self.nickname and self.user and not self.password:
            yield self.send_msg("464", "You must supply a password")
        elif self.nickname and self.user and self.password:
            self.server.clients_by_name[self.nickname] = self
            yield self.send_welcome()
            login = yield self.server.rook.login(self, self.nickname, self.password)
            if isinstance(login, str):
                yield self.send_error(login)
                yield self.send_msg("QUIT", actor=self)
                self.stream.close()
                return
            self.rook = login
            logging.info("Logged into RookChat successfully")
            errorcheck(self.cmd_list())

    @coroutine
    def send_welcome(self):
        yield self.send_msg("001", "Welcome to IRookC, {}".format(self.nickname))
        yield self.send_msg("002", "Your host is {}, running IRookC-{}".format(self.server.server_name, __VERSION__))
        yield self.send_msg("003", "This server was created at some point")
        yield self.send_msg("004", self.server.server_name, "IRookC-" + __VERSION__)
        yield self.cmd_motd()

    @coroutine
    def cmd_motd(self):
        yield self.send_msg("375", "- {} Message of the day -".format(self.server.server_name))
        for line in self.server.motd:
            yield self.send_msg("372", line)
        yield self.send_msg("376", "End of /MOTD command")

    @coroutine
    def handle_cmd_unknown(self, cmd, *args):
        logging.warning("Unknown command received: %s %s", cmd, args)

    @coroutine
    def cmd_nick(self, nick):
        if self.nickname:
            # changing nickname not allowed
            logging.info("Ignored NICK %s", nick)
            return
        logging.info("Nickname: %s", nick)
        self.nickname = nick
        yield self.check_login()

    @coroutine
    def cmd_pass(self, password):
        if self.password:
            return
        logging.info("Got password")
        self.password = password
        yield self.check_login()

    @coroutine
    def cmd_user(self, user, mode, meh, realname):
        if self.user:
            return
        logging.info("User: %s", user)
        self.user = user
        self.display_host = user
        yield self.check_login()

    @coroutine
    def cmd_quit(self, msg=""):
        logging.info("Quit")
        self.stream.close()

    @coroutine
    def cmd_userhost(self, *users):
        # TODO: away handling
        msg = []
        for i in users:
            if i in self.server.clients_by_name:
                msg.append("{}=+{}".format(i, self.server.clients_by_name[i].display_host))
        yield self.send_msg("302", " ".join(msg))

    @coroutine
    def cmd_ping(self, arg):
        yield self.send_msg("PONG", arg, target=self.server.server_name)

    re_action = re.compile(r"\x01ACTION (.*)\x01")
    @coroutine
    def cmd_privmsg(self, dest, msg):
        if dest not in self.rooms:
            dest = User(dest)
            # Here we have a slight problem, because in IRC private messages
            # aren't tied to a single room. There are probably more intelligent
            # strategies for selecting a room that could be used, based on
            # activity and the like, but for the moment we're just going to
            # look through the rooms until we find one with the right person in
            # and then send the message. And if they're not there we try using
            # rmsg instead.
            r = None
            for room in self.rooms.values():
                if dest in room.userlist:
                    yield room.send_msg("/msg " + dest + " " + msg)
                    return
                r = room
            if r is None:
                # we're not in any rooms so we can't even rmsg
                yield self.send_error("You can't send private messages without being in at least one room.")
                return
            yield r.send_msg("/rmsg " + dest + " " + msg)
            return
        m = self.re_action.match(msg)
        if m:
            yield self.rooms[dest].send_msg("/me " + m.group(1))
        else:
            yield self.rooms[dest].send_msg(msg)

    @coroutine
    def cmd_who(self, target):
        if not target.startswith("#"):
            # TODO: implement this for single users
            logging.warning("Unhandled WHO command sent for %s", target)
            return
        if self.rooms[target].userlist is None:
            # the "right" thing to do here would be to cleanly wait for the
            # userlist to become available and then send the message. that
            # being said...
            logging.debug("WHO before userlist available, delaying")
            IOLoop.current().call_later(1, self.cmd_who, target)
            return
        for i in self.rooms[target].userlist:
            # TODO: handle Here/Gone correctly
            yield self.send_msg("352", target, i, i, self.server.server_name, i, "H", "0 {}".format(i))
        yield self.send_msg("315", target, "End of /NAMES list")

    @coroutine
    def cmd_whois(self, server, target=""):
        if not target:
            target = server
        # TODO: something neater for searching all channels here?
        channels = []
        for name, chan in self.rooms.items():
            if target in chan.userlist:
                channels.append(name)
        if not channels:
            yield self.send_msg("401", target, "No such nick/channel")
        else:
            yield self.send_msg("311", target, target, target, "*", target, colon=True)
            yield self.send_msg("312", target, self.server.server_name, "IRookC", colon=True)
            yield self.send_msg("319", target, " ".join(channels), colon=True)
        yield self.send_msg("318", target, "End of /WHOIS list")

    @coroutine
    def cmd_mode(self, *args):
        if not args:
            return
        if len(args) > 1:
            # mode change, ignore
            logging.debug("ignored mode change")
        if args[0] in self.rooms:
            yield self.send_msg("324", args[0], "+", colon=False)
        else:
            # this doesn't check to see if the user actually exists yet
            yield self.send_msg("221", "+i", colon=False)

    @coroutine
    def cmd_list(self, *args):
        # we just ignore arguments and list all the rooms, since there's rarely
        # very many
        yield self.rook.get_room_list()

    @coroutine
    def cmd_join(self, channels):
        for chan in channels.split(","):
             room = yield self.rook.join_room(chan)
             if room is None:
                 continue
             self.rooms[room.name] = room

    @coroutine
    def cmd_part(self, channel, quitmsg=""):
        yield self.rooms[channel].leave()

    @coroutine
    def cmd_roll(self, *args):
        room = self.get_most_recent_room()
        if not room:
            yield self.send_error("You can't roll dice without being in at least one room.")
        yield room.send_msg(" ".join(["/roll"] + list(args)))

    @coroutine
    def cmd_rollp(self, *args):
        room = self.get_most_recent_room()
        if not room:
            yield self.send_error("You can't roll dice without being in at least one room.")
        yield room.send_msg(" ".join(["/rollp"] + list(args)))

    def get_most_recent_room(self):
        # for irc messages that we can't tie to a specific room, as a hack use
        # the one we spoke in most recently
        if not self.rooms:
            return None
        rooms = sorted(self.rooms.values(), key=lambda x: x.last_spoke)
        return rooms[-1]
