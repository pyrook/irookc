import pycares
from tornado.gen import coroutine, Task
from tornado.platform.caresresolver import CaresResolver

class DNSException(Exception):
    pass

class Resolver(CaresResolver):
    def adapt(self, host, callback):
        return self.channel.gethostbyaddr(host, callback)

    @coroutine
    def gethostbyaddr(self, name):
        f = yield Task(self.adapt, name)
        res, error = f.args
        if error:
            raise DNSException("C-Ares returned error {}: {}".format(error, pycares.errno.strerror(error)))
        return res.name
