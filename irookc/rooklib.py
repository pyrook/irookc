import collections
import datetime
import functools
import logging
import re
import time
from urllib.parse import parse_qs, urlencode, urlparse

from lxml import html
from tornado.gen import coroutine
from tornado.httpclient import AsyncHTTPClient, HTTPError, HTTPRequest
from tornado.ioloop import PeriodicCallback

from .helpers import errorcheck, User
from . import __VERSION__

def request(url, body, **kwargs):
    body = urlencode(body).encode("iso-8859-1", errors="replace")
    return HTTPRequest(url, "POST", body=body, **kwargs)

class Rook:
    def __init__(self, url):
        self.url = url + "index.cgi"
        self.url_path = url

        # configure HTTP client
        # streaming_callback will be used on streams
        AsyncHTTPClient.configure(None, max_clients=100, defaults={
                "user_agent": "IRookC/{}".format(__VERSION__),
                "headers": {"Content-Type": "application/x-www-form-urlencoded"},
            })

    @coroutine
    def login(self, controller, username, password):
        logging.info("Logging in %s", username)
        req = request(self.url, {
                "action": "login",
                "name": username,
                "pass": password,
            })
        try:
            res = yield AsyncHTTPClient().fetch(req)
        except HTTPError:
            logging.warning("Login request failed", exc_info=True)
            return "HTTP request failed - server may be down"
        tree = html.parse(res.buffer)
        err = tree.getroot().cssselect('body div[id="content"] font[color="red"]')
        if err:
            msg = err[0].text_content() + " " + err[1].text_content()
            logging.info("Login failed: %s", msg)
            return msg
        return Connection(self.url_path, controller, username, password)


class Connection:
    def __init__(self, url, controller, username, password):
        self.url = url + "index.cgi"
        self.url_path = url
        self.controller = controller
        self.username = username
        self.password = password

        self.rooms = []
        self.known_rooms = {}

    @coroutine
    def get_room_list(self, quiet=False):
        # TODO: detect difference between main and user rooms here, and don't
        # add the ID number to main rooms
        roomlist_req = request(self.url, {
                "action": "rooms",
                "language": "english_american",
                "name": self.username,
                "pass": self.password,
            })
        try:
            roomlist_res = yield AsyncHTTPClient().fetch(roomlist_req)
        except HTTPError:
            logging.warning("Room list request failed", exc_info=True)
            return "HTTP request failed - server may be down"
        logging.info("Got room list")
        tree = html.parse(roomlist_res.buffer)

        rooms_source = tree.getroot().xpath("/html/body/div/table[2]/tr/td/table/tr")

        rooms = []
        for i in rooms_source:
            room = {}
            room["name"] = i.xpath('td[1]/table/tr/td/b')[0].text.strip().strip(",")
            room["id"] = i.forms[0].fields["room"]

            props = i.xpath("td/table/tr/td/b/font | td/table/tr/td/font")
            for j in range(0, len(props), 2):
                prop_key = props[j].text_content().strip().strip(":").lower()
                prop_val = props[j+1].text_content().strip()
                room[prop_key] = prop_val

            rooms.append(room)

        self.known_rooms = {}
        if not quiet:
            yield self.controller.send_msg("321", "Channel", "Users Name")
        for i in rooms:
            if i["users"] == "No one present.":
                c = 0
            else:
                c = i["users"].count(",") + 1
            name = "#" + i["name"].lower().replace(" ", "_") + i["id"]
            self.known_rooms[name] = i["id"]
            if not quiet:
                yield self.controller.send_msg("322", name, str(c), i.get("topic", ""))
        if not quiet:
            yield self.controller.send_msg("323", "End of /LIST")

    @coroutine
    def match_room(self, roomn=None, roomc=None):
        for tries in range(2):
            if roomc:
                # iterate over known rooms to find matching ID
                for n, i in self.known_rooms.items():
                    if i == roomc:
                        room, rid = n, i
                        return room, rid
            else:
                room = roomn.lower()
                if room in self.known_rooms:
                    rid = self.known_rooms[room]
                    return room, rid
            if tries == 0:
                # grab room list and try again
                yield self.get_room_list(quiet=True)
        yield self.controller.send_msg("403", room, "Couldn't join: room does not exist")

    @coroutine
    def join_room(self, room):
        # AndChat "helpfully" prepends # to /joined rooms if it's not there,
        # so doing "/join 0" actually sends "JOIN #0".
        m = re.match(r"#?(\d+)$", room)
        if m:
            room, rid = yield self.match_room(roomc=m.group(1))
        else:
            room, rid = yield self.match_room(roomn=room)

        join_req = request(self.url, {
                "action": "enter",
                "room": rid,
                "name": self.username,
                "pass": self.password,
                "ts": str(time.time()),
            })
        try:
            join_res = yield AsyncHTTPClient().fetch(join_req)
        except HTTPError:
            logging.warning("Room join request failed", exc_info=True)
            yield self.controller.send_msg("403", room, "Couldn't join: HTTP error")
            return
        logging.info("Joined room")
        yield self.controller.send_msg("JOIN", target=room, actor=self.controller)
        room_obj = Room(self.controller, self, rid, room)
        self.rooms.append(room_obj)
        return room_obj


class Room:
    def __init__(self, controller, connection, rid, name):
        self.controller = controller
        self.connection = connection
        self.id = rid
        self.name = name

        self.chatstream_started = 0
        self.finished = False
        self.data_buffer = b""
        self.last_message_ts = datetime.datetime(1900, 1, 1)
        self.last_data_ts = 0
        self.last_spoke = 0
        self.received_history = collections.deque(maxlen=10)
        self.sent_history = collections.deque(maxlen=10)
        self.topic = None
        self.userlist = None

        errorcheck(self.chatstream())
        errorcheck(self.get_userlist())
        self.user_list_timer = PeriodicCallback(self.get_userlist, 20 * 1000)
        self.user_list_timer.start()

    @coroutine
    def get_userlist(self):
        req = request(self.connection.url, {
                "action": "userlist",
                "room": self.id,
                "name": self.connection.username,
                "pass": self.connection.password,
                "ts": str(time.time()),
                "language": "english_american",
            })
        try:
            res = yield AsyncHTTPClient().fetch(req)
        except HTTPError:
            logging.warning("Stream request failed", exc_info=True)
            return
        tree = html.parse(res.buffer)
        topic = tree.getroot().xpath("//p/b/font")
        if len(topic) >= 2:
            if topic[0].text_content().strip() == "Topic:":
                yield self.set_topic(topic[1].text_content().strip())
        users = tree.getroot().xpath("//table/tr/td")
        userlist = set()
        for u in users:
            username = u.xpath("b//font")[0].text_content().strip()
            userlist.add(username)
        yield self.update_userlist(userlist)

    @coroutine
    def update_userlist(self, userlist=None, join=None, leave=None):
        if userlist:
            userlist = {User(i) for i in userlist}
        if self.userlist is None and userlist is not None:
            # first join, send names list rather than a join statement for
            # everybody
            self.userlist = userlist
            yield self.controller.send_msg("353", "@", self.name, " ".join(self.userlist))
            yield self.controller.send_msg("366", self.name, "End of /NAMES list.")
            return
        if self.userlist is None:
            # this is probably historical joins/leaves in the chatstream before
            # we've received the first userlist, so just ignore it
            return
        join = {User(i) for i in join} if join else set()
        leave = {User(i) for i in leave} if leave else set()
        if userlist:
            join |= userlist - self.userlist
            leave |= self.userlist - userlist
        for i in join:
            yield self.controller.send_msg("JOIN", actor=User(i), target=self.name)
        for i in leave:
            yield self.controller.send_msg("PART", actor=User(i), target=self.name)
        self.userlist = (self.userlist - leave) | join

    @coroutine
    def set_topic(self, topic, setter=None):
        if setter:
            yield self.controller.send_msg("TOPIC", topic, target=self.name, actor=setter)
        elif topic != self.topic:
            yield self.controller.send_msg("332", self.name, topic)
        self.topic = topic

    @coroutine
    def chatstream(self, force=False):
        # I would prefer to just make this loop over the fetch() method, but
        # there doesn't seem to be a way to cancel ongoing requests, so we need
        # to be able to step into the chatstream routine from multiple places,
        # avoid starting too many chatstreams when that happens, and accept
        # that it may occasionally take a few minutes for all connections to
        # get cleaned up when someone leaves.
        if not force and ((time.time() - self.chatstream_started) < (10 * 60 - 10)):
            logging.debug("chatstream start aborted by time limit")
            return
        logging.debug("starting chatstream")
        self.chatstream_started = time.time()
        req = request(self.connection.url_path + "stream.cgi", {
                "action": "stream",
                "room": self.id,
                "name": self.connection.username,
                "pass": self.connection.password,
                "ts": str(time.time()),
                "language": "english_american",
            },
            connect_timeout = 30,
            request_timeout = 10 * 60 + 10, # streams run ten minutes
            streaming_callback = self.streamed_data)
        force_next = False
        try:
            yield AsyncHTTPClient().fetch(req)
        except HTTPError:
            logging.warning("Stream request failed", exc_info=True)
            if (time.time() - self.chatstream_started) < 40:
                # this is likely to be a connection timeout, rather than a
                # total stream runtime expiry
                force_next = True
            # otherwise, either a new stream will be started organically
            # because the ten minutes are up, or a timeout has already been
            # detected and a new stream started, in which case we don't want
            # this starting another one
        if not self.finished:
            errorcheck(self.chatstream(force_next))

    @coroutine
    def streamed_data(self, data):
        if self.finished:
            return
        self.last_data_ts = time.time()
        blocks = (self.data_buffer + data).split(b"\n")
        *blocks, self.data_buffer = blocks
        for b in blocks:
            try:
                line = b.decode("iso-8859-1").strip()
            except UnicodeDecodeError:
                line = b.decode("utf8", errors="replace").strip()
            m = re.match(r"<!-- (\d{1,2}/\d{1,2}/\d{4}, \d{2}:\d{2}:\d{2}) -->", line)
            if not m:
                # script, other miscellaneous crap (hopefully)
                continue
            ts = datetime.datetime.strptime(m.group(1), "%m/%d/%Y, %H:%M:%S")
            if ts < self.last_message_ts:
                # refreshing or something
                continue
            if ts == self.last_message_ts:
                # check our received buffer for duplicates
                if line in self.received_history:
                    continue
            self.last_message_ts = ts
            self.received_history.append(line)
            try:
                yield self.handle_line(line)
            except Exception:
                logging.exception("")

    nameblock = r"[A-Za-z0-9_-]+"
    namegroup = r"(" + nameblock + r")"
    re_join = re.compile(namegroup + r" has entered.")
    re_leave = re.compile(namegroup + r" has left.")
    re_pubmsg = re.compile(namegroup + r": (.*)")
    re_timestamp = re.compile("\((?:\d{1,2}/\d{1,2}/\d{4}, )\d{2}:\d{2}:\d{2}\)")
    re_action = re.compile(r"\* " + namegroup + r" (.*)")
    remoteblock = r"(?:\[remote\] )?"
    re_sent_privmsg = re.compile(remoteblock + r"\[->((?:" + nameblock + r", )*" + nameblock + r")\] (.*)")
    re_recv_single_privmsg = re.compile(remoteblock + r"\*" + namegroup + r"\* (.*)")
    re_recv_multi_privmsg = re.compile(remoteblock + r"\*" + namegroup + r"->((?:" + nameblock + r", )*" + nameblock + r")\* (.*)")
    @coroutine
    def handle_line(self, line):
        #logging.debug("handling line %s", line)
        # TODO: check sent history to avoid message duplication
        tree = html.fragment_fromstring(line, True)
        urls = tree.xpath("//a")
        for a in urls:
            link = a.attrib["href"]
            up = urlparse(link)
            if link.startswith("index.cgi?action=link&url="):
                qdict = parse_qs(up.query)
                a.text = qdict["url"][0]
            # don't convert other links to avoid URLs with the password in them
            # appearing in the stream
        text = html.tostring(tree, method="text", encoding=str).strip()
        # I'm being lazy and just regexing the lines here rather than trying to
        # do anything clever with the actual HTML, at least as far as possible.
        # Note: we avoid sending the client most of its own messages to avoid
        # duplication (in the case of normal messages) or getting it terribly
        # confused (in the case of join/leave messages).
        for msgtype in ("join", "leave", "action", "pubmsg", "sent_privmsg", "recv_single_privmsg", "recv_multi_privmsg"):
            m = getattr(self, "re_" + msgtype).match(text)
            if m:
                handled = yield getattr(self, "msg_" + msgtype)(m, text, tree)
                if handled:
                    return
        # fallback for unknown message: just notice it to the user
        yield self.controller.send_msg(
                "NOTICE",
                text,
                target=self.name)

    def drop_own_username(fn):
        @coroutine
        @functools.wraps(fn)
        def inner(self, match, text, tree):
            if match.group(1).lower() == self.connection.username.lower():
                return True
            val = yield fn(self, match, text, tree)
            return val
        return inner

    @drop_own_username
    @coroutine
    def msg_join(self, match, text, tree):
        yield self.update_userlist(join=set([match.group(1)]))
        return True

    @drop_own_username
    @coroutine
    def msg_leave(self, match, text, tree):
        yield self.update_userlist(leave=set([match.group(1)]))
        return True

    @drop_own_username
    @coroutine
    def msg_pubmsg(self, match, text, tree):
        text = self.strip_timestamp(tree)
        msg = self.re_pubmsg.match(text).group(2)
        yield self.controller.send_msg("PRIVMSG", msg, actor=User(match.group(1)), target=self.name)
        return True

    @drop_own_username
    @coroutine
    def msg_action(self, match, text, tree):
        # I was going to verify that the detected username is actually in the
        # room here, but this causes problems when reading scrollback from
        # before the user has actually joined, because we don't have an
        # accurate picture of who's actually in the room. Coming up with a
        # solution goes on the maybe-todo list I guess.
        text = self.strip_timestamp(tree)
        msg = self.re_action.match(text).group(2)
        yield self.controller.send_msg("PRIVMSG", "\x01ACTION {}\x01".format(msg), actor=User(match.group(1)), target=self.name)
        return True

    @coroutine
    def msg_sent_privmsg(self, match, text, tree):
        # just eat these for the moment
        return True

    @coroutine
    def msg_recv_single_privmsg(self, match, text, tree):
        yield self.controller.send_msg("PRIVMSG", match.group(2), actor=User(match.group(1)), target=self.connection.username)
        return True

    @coroutine
    def msg_recv_multi_privmsg(self, match, text, tree):
        # IRC doesn't have a neat way of handling these. The solution is
        # probably to create pseudo-rooms for multi-way private conversations,
        # but that's an implementation for another day.
        yield self.controller.send_msg("PRIVMSG", "[->{}] {}".format(match.group(2), match.group(3)), actor=User(match.group(1)), target=self.connection.username)
        return True

    def strip_timestamp(self, tree):
        ts = tree.xpath("//font[size]")
        if ts:
            ts_text = ts[-1].text_content().strip()
            if self.re.match(re_timestamp, ts_text):
                tree.remove(ts[-1])
        return html.tostring(tree, method="text", encoding=str).strip()

    @coroutine
    def leave(self, confirm=True):
        self.finished = True
        self.user_list_timer.stop()
        logging.info("Leaving %s", self.id)
        req = request(self.connection.url, {
                "action": "leave",
                "room": self.id,
                "name": self.connection.username,
                "pass": self.connection.password,
                "ts": str(time.time()),
            })
        try:
            res = yield AsyncHTTPClient().fetch(req)
        except HTTPError:
            logging.warning("Room leave request failed", exc_info=True)
        else:
            if confirm:
                yield self.controller.send_msg("PART", target=self.name, actor=self.controller)
        # tear down active streams, recurring tasks, etc

    @coroutine
    def send_msg(self, msg):
        logging.debug("Sending message")
        self.last_spoke = time.time()
        req = request(self.connection.url, {
                "action": "post",
                "room": self.id,
                "name": self.connection.username,
                "pass": self.connection.password,
                "submit_id": str(time.time()),
                "msg": msg,
            })
        try:
            res = yield AsyncHTTPClient().fetch(req)
        except HTTPError:
            logging.warning("Message send request failed", exc_info=True)
            yield self.controller.send_error("Message send failed")
